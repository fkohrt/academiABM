[![License: Apache License 2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://choosealicense.com/licenses/apache-2.0/)
[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)
[![CRAN status](https://www.r-pkg.org/badges/version/academiABM)](https://CRAN.R-project.org/package=academiABM)

# Modeling openness in academia with an agent-based model

## Model summary

The purpose of this model is to create a working model of academia that can provide insight into the effect of introducing openness to science.
Academia is modeled at the level of researchers that act as agents.

### Description

- An original study tests a previously untested hypothesis concerning the standardized mean difference between a treatment and a control group using a two-sample NHST with a two-tailed (but directed) alternative hypothesis (effectively being a one-tailed t-test against alpha/2).
- The alternative to original studies are redoing studies, either reproduction or replication studies. Replications choose an original study and try to validate its results by collection a separate sample. Reproductions choose an original or a replication study and re-calculate its analysis. Redoing studies test directed hypothesis using a one-tailed test and a significance threshold of alpha.
- The model discerns multiple effect sizes:
  - true effect size: unknown to agents, used to create sampled effect size
  - sampled effect size: deviates from true effect size due to sampling error and p-hacking, unknown to agents (because they are unaware of analysis errors), used to create observed effect size
  - observed effect size: deviates from sampled effect size due to analysis errors, known to agents
  - reported effect size: deviates from observed effect size due to corrections after publication, known to agents
- Agents can make three kinds of errors: Sampling errors during data collection, analysis errors during data analysis and p-hacking after data analysis.
  - Reproductions can only find inconsistencies due to analysis errors, but not sampling errors or p-hacking.
  - Replications can find inconsistencies due to all three types of errors.
  - Replications can make analysis and sampling errors, reproductions can make no errors.
  - This is a simplified model of the various errors that may occur during research. In particular, all p-hacking operates on the raw data and can never be detected by reproductions and all analysis errors do not change raw data and can in principle be detected by reproductions.
- One time step represents one month.
- One agent represents one researcher.

| Summary                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Implementation Details                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Code Reference |
| ------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------- |
| Determine start of study        | Only agents with no ongoing study can start a new one.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                |
| Choose study type               | Agents choose between three study types: original, reproduction and replication. They will redo an original study using a reproduction or replication with individual probabilities `prob_reproduce` / `prob_replicate`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                |
| Choose original study           | For replication studies, all published original studies are available. For reproductions, only original and replication studies that were not already reproduced by a published reproduction are considered, and closed studies must additionally provide their sampled data which only happens with a probability of `prob_closed_has_data`. Agents have an individual preference for open vs. closed papers via `odds_repeat_open`.                                                                                                                                                                                                                                                               | Numerical example: From all published original studies, suppose one is open and nine are closed. Setting `odds_repeat_open = 2` will double the odds of open papers, resulting in a probability of (2 × 1) / (9 + 2 × 1) = 2/11 to select an open paper for redoing.                                                                                                                                                                                                                                   |                |
| Determine sample size           | The sample size is the number of independent observations in each of two groups (treatment and control). If agents have an individual `target_power` set, the necessary sample size will be calculated assuming a medium effect if they are conducting original research and assuming the original reported effect size if they are conducting a replication. If no `target_power` is set, the sample size will be drawn from a distribution of sample sizes found in the literature. Reproductions have the same sample size as the original study as they are not making new observations.                                                                                                        | Medium published effects have a Pearson product-moment correlation of 0.19 as suggested by Gignac & Szodorai ([2016](https://doi.org/10.1016/j.paid.2016.06.069)). The distribution of sample sizes follows a truncated inverse gamma distribution as found by Carter et al. ([2019](https://doi.org/10.1177/2515245919847196)) and is multiplied with a factor of 2.38912 derived from a study by Fraley et al. ([2022](https://doi.org/10.1177/25152459221120217)).                                  |                |
| Create sample data              | Creating sample data encompasses determining the existence and size of a true effect as well as simulating a random draw from two populations characterized by that true effect.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | Sampling is only simulated and the result is summarized as standardized mean difference (Cohen's d) between treatment and control.                                                                                                                                                                                                                                                                                                                                                                     |                |
| Determine true effect existence | An original study is about a (non-zero) true effect with a probability of `p_H1`. Redoing studies have the same true effect as their originals.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | The prior probability of testing a (non-zero) true effect is estimated to be 0.1 at maximum [citation needed].                                                                                                                                                                                                                                                                                                                                                                                         |                |
| Determine true effect size      | Effect sizes are standardized mean differences (Cohen's d) between a treatment and a control group. If an original study is about a (non-zero) true effect, its size follows the distribution of effect sizes found in the literature, but is halved in magnitude as the literature values are likely overestimations. Redoing studies have the same true effect as their originals.                                                                                                                                                                                                                                                                                                                | The true effect size is calculated by (a) drawing a correlation coefficient from a gamma distribution (as suggested by the results of Richard et al., [2003](https://doi.org/10.1037/1089-2680.7.4.331)) which is modeled using quartiles from Gignac & Szodorai ([2016](https://doi.org/10.1016/j.paid.2016.06.069)) and truncated to [0.05, 1], (b) converting it to Cohen's d and (c) halving it. As such, non-zero true effect sizes will always be positive and have a minimum value of d ≈ 0.05. |                |
| Determine sampled effect size   | If the study is about a (non-zero) true effect, a t-statistic will be drawn from a noncentral t-distribution, otherwise it will be drawn from a central t-distribution. It is then converted to Cohen's d.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | The t-distributions have the degree of freedoms according to a t-test of two independent samples (2 × n - 2). The parameter for the noncentral t-distribution for same-size samples is calculated as √(n/2) × d.                                                                                                                                                                                                                                                                                       |                |
| Perform analysis                | Agents do not have access to the sampled effect size right away, as they need to conduct an analysis of the sampled data before. This analysis will lead to an observed effect size which is only equal to the sampled effect size if they do not make analysis errors, which can occur for original and replication studies (but not for reproduction studies). For original studies the observed effect is used to calculate a p-value corresponding to a two-sample NHST with a two-tailed (but directed) alternative hypothesis, making it in effect a one-tailed test against alpha/2. Redoing studies test directed hypothesis using a one-tailed test and a significance threshold of alpha. |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                |
| Attempt p-hacking               | Some of the agents that conduct original studies will attempt p-hacking, which can only work if the observed p-value is close to significant.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | If alpha < p_value < 2 × alpha, then p_value := Uniform(0, p_value); p-hacking will also manipulate the sample data, hence a reproduction cannot detect it.                                                                                                                                                                                                                                                                                                                                            |                |
| Determine confirmation status   | This model makes the assumption that reviewers and editors determine the confirmation status of a reproduction or replication study by comparing its significance with the original study's significance. Confirmation status only affects the publication status and is no fundamental property of a scientific paper but merely a criterion that is believed to be widely applied.                                                                                                                                                                                                                                                                                                                |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                |
| Determine openness              | A paper will be made open with a probability varying between agents and depending on the paper type (properties `prob_open_original` and `prob_open_repeating`).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                |
| Determine duration              | Studies vary in the time they take to complete them, which depends on three features: Original vs. redoing, open vs. closed and sample size.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | Original studies take two months (as a baseline) while redoing studies take only one month. Making a paper open takes one extra month. Ten observations (five per group) take one day. Redoing open papers takes half the time as non-open papers, but at least one time step.                                                                                                                                                                                                                         |                |
| Determine publication status    | Either "published" or "unpublished", depending on the significance (for an original study) or the confirmation status (for a redoing study).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | `prob_pub_sig = 1.0`, `prob_pub_null = 0.009`, `prob_pub_conf = 0.6`, `prob_pub_disconf = 0.7`. A study is to be considered "pending" for the time steps of its duration, starting in the time step of the start of the study.                                                                                                                                                                                                                                                                         |                |
| Correct original                | Correct reproductions of studies with analysis errors that are published will lead to a correction of the original work, such that effect size, p-value and significance are adjusted.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |                |

### Flowchart of statistical tests

```mermaid
flowchart TD
    subgraph sample_size
        is_reproduction(["Is reproduction?"]) -->|FALSE| no_target_power
        is_reproduction -->|TRUE| original_sample_size{{"≔ (original value)"}}
        no_target_power(["is.na(target_power)"]) -->|TRUE| sample_size_distribution{{"~ IG(1.153, 21.63)[5,1905] × 2.4"}}
        no_target_power -->|FALSE| is_replication(["Is replication?"])
        is_replication -->|FALSE| original_sample_size_calculation{{"≔ power.t.test(<br>medium_delta,<br>alpha,<br>target_power,<br>strict = TRUE)$n"}}
        is_replication -->|TRUE| replication_sample_size_calculation{{"≔ power.t.test(<br>(original reported effect size),<br>alpha,<br>target_power,<br>strict = TRUE)$n"}}
    end
    subgraph has_true_effect
    original_sample_size --> original_has_true_effect{{"≔ (original value)"}}
        sample_size_distribution & original_sample_size_calculation & replication_sample_size_calculation --> is_replication2(["Is replication?"])
        is_replication2 -->|FALSE| has_true_effect_distribution{{"≔ U(0, 1) < p_H1"}}
        is_replication2 -->|TRUE| original_has_true_effect
    end
    subgraph true_effect_size
        has_true_effect_distribution -->|TRUE| true_effect_distribution{{"~ G(2.223, 0.097)[0.05,1] |><br>r_to_d() |><br>halve()"}}
        has_true_effect_distribution -->|FALSE| null_effect{{"≔ 0.0"}}
        original_has_true_effect --> original_true_effect{{"≔ (original value)"}}
    end
    subgraph sampled_effect_size ["sampled_effect_size <em>(data collection)</em>"]
        true_effect_distribution --> noncentral_t_distribution{{"~ T(df, ncp)"}}
        null_effect --> central_t_distribution{{"~ T(df)"}}
        original_true_effect --> repeating_type(["Replication or reproduction?"])
        repeating_type --> |replication| has_true_effect2(["Has true effect?"])
        repeating_type --> |reproduction| original_sampled_effect_size{{"≔ (original value)"}}
        has_true_effect2 -->|TRUE| noncentral_t_distribution
        has_true_effect2 -->|FALSE| central_t_distribution
        noncentral_t_distribution & central_t_distribution --> sampled_effect_distribution{{"t_to_d()"}}
    end
    subgraph observed_effect_size ["observed_effect_size <em>(data analysis)</em>"]
        sampled_effect_distribution --> has_analysis_error(["Has analysis error?"])
        original_sampled_effect_size --> nonerror_distribution
        has_analysis_error -->|TRUE| error_distribution{{"~ G(2.223, 21.63)[0.05,1] |><br>r_to_d()"}}
        has_analysis_error -->|FALSE| nonerror_distribution{{"≔ sampled_effect_size"}}
    end
    subgraph observed_t_statistic
        error_distribution & nonerror_distribution --> t_statistic{{"≔ d_to_t(observed_effect_size)"}}
    end
    subgraph observed_p_value
        t_statistic --> p_value{{"≔ P(X ≥ observed_t_statistic)"}}
    end
    subgraph is_significant
        p_value --> study_type(["Original or repeating study?"])
        study_type -->|original| original_is_significant{{"≔ observed_p_value ≤ alpha/2"}}
        study_type -->|repeating| repeating_is_significant{{"≔ observed_p_value ≤ alpha"}}
    end
    subgraph p_hacking
       original_is_significant --> do_p_hacking(["U(0, 1) < prob_attempt_p_hacking &<br>alpha < observed_p_value < 2 * alpha"])
       do_p_hacking -->|TRUE| hacked_p_value["hacked_observed_p_value ≔<br>U(0, observed_p_value)"]
       hacked_p_value --> hacked_significance["hacked_is_significant ≔<br>hacked_observed_p_value ≤ alpha"]
       hacked_significance --> hacked_observed_effect_size["hacked_observed_effect_size ≔<br>hacked_observed_p_value |><br>p_to_t() |><br>t_to_d()"]
       hacked_observed_effect_size --> hacked_sampled_effect_size["hacked_sampled_effect_size ≔<br>sampled_effect_size +<br>(hacked_observed_effect_size -<br>observed_effect_size)"]
    end
    subgraph confirmation_status
        repeating_is_significant --> confirmation_status2{{"≔ is_significant ==<br>original_is_significant"}}
    end
    subgraph true_power
        confirmation_status2 --> true_power2{{"≔ power.t.test(<br>sample_size,<br>true_effect_size,<br>alpha,<br>strict = TRUE)$power"}}
        do_p_hacking -->|FALSE| true_power2
        hacked_sampled_effect_size --> true_power2
    end
    subgraph assumed_power
        true_power2 --> study_type2(["Study type"])
        study_type2 -->|original| assumed_power_original{{"≔ power.t.test(<br>sample_size,<br>medium_delta,<br>alpha,<br>strict = TRUE)$power"}}
        study_type2 -->|reproduction| assumed_power_reproduction{{"≔ (original value)"}}
        study_type2 -->|replication| assumed_power_replication{{"≔ power.t.test(<br>sample_size,<br>(original reported effect size),<br>alpha,<br>strict = TRUE)$power"}}
    end
```

## Old behavior description

- Per time step, agents perform a study with a certain global probability (`prob_investigate`).
<!-- TODO: haben wir nicht diesen Parameter gestrichen?-->
  - They may only attempt a new study if they have no other study ongoing.
- The study is either testing a new hypothesis or is a replication with a certain probability.
  - Original studies take two month at minimum, while the base duration of replication studies is only one month.
<!-- TODO: Hier Unterscheidung replication/reproduction einführen -->

- If it's a replication, agents either choose an open or a non-open target paper, with a certain probability.
  - An open paper might get more or less often selected for replication via `odds_replicate_open`.
- Agents either have a target power they want to maintain, which will determine their study's sample size. Or they will draw a sample size value from a typical distribution of sample sizes found in the psychological literature.
- Depending on the true effect size and their study's sample size, agents get a significant result or not.
  - Higher sample size means higher power and thus more positive results for true hypotheses, but also comes at the cost of a longer time to produce a paper (100 observations/month per group, i.e. treatment and control).
<!-- TODO: i.e., 50 each in treatment and control group -->  
- Agents have separate preferences to make their original and replication studies open.
  - Producing an open paper requires extra effort. In the current simulation, it takes one additional month.
- The probability that agents get to publish their results depends on the result (significant/n.s.) for new hypotheses or the confirmation status (confirmative/disconfirmative) for replication studies.
  - Significant original and disconfirming replication studies get published easier.

### Possible data collection

- aggregate over vs. group by...
  - time: all or only new data
  - type
    - agents: publishing open vs. not, replicating often vs. rarely, published many vs. few papers
    - papers: non-replications vs. replications, open vs. non-open, published vs. not

## Installation (first time)

TODO: Add some introductory sentences, why the installation is so complicated ...

```r
# First, build documentation without compilation
# because compilation requires a NAMESPACE file
withr::with_options(
  list(warn = 1),
  roxygen2::roxygenise(load_code = roxygen2::load_source)
)

# Now compile code
withr::with_options(
  list(warn = 1),
  pkgbuild::compile_dll(force = TRUE)
)

# Finally, build documentation again.
# If there are any warnings, resolve them.
devtools::document()
devtools::install()
```

## Updating installation during development

TODO: Add some introductory sentences, why the installation is so complicated ...

```r
devtools::document()
devtools::install()
```


## Usage

<!-- TODO: Add instructions how to install the package. add NAMESPACE to git repo (why excluded?)  -->

### Run a single model

<!-- TODO: How can you inspect the properties of an R6 object? str() does not work  -->

```r
sim1 <- simulation$new()
sim1$run()
sim1$plot()
print(sim1$report_total)
```


### Conducting an experiment

```r

e <- experiment$new()

# TODO: This parameter does not exist any more.
e$sweep("prob_investigate", seq(0, 1, 0.1))
e$report |> print()

# e$report$get() |>
#   summarytools::dfSummary(plain.ascii = FALSE,
#                           valid.col = FALSE,
#                           style = "grid") |>
#   print(method = "render", footnote = NA)
```

## Developer Notes

- Next tasks
  - include calibration code
  - Phases (before vs. after 2012)
    - Will directed hypotheses be tested using two-tailed tests?
    - Drawn sample sizes
    - Publication bias
  - check that only published papers end up in an agent's `failed_replications` counter
  - add paper id
  - test if confirmation works
  - evolutionary model
  - speed up ifelse and which
  - add payoff in some sense*
  - terminology: replication vs. reproduction
- Possible performance improvements
  - replace paper entities by four additional entries (amount of true/false positives/negatives) per agent
  - [Long Vectors](https://stat.ethz.ch/R-manual/R-patched/library/base/html/LongVectors.html)
- General Topics
  - Testing
  - Multiple runs / replications
  - Parallelization
    - `parallelly`
    - type of future: `multicore`
    - `foreach` / `doFuture`
  - Data collection / reporting
  - Visualization
  - Progress
  - Serialization
  - Debugging
  - Caching
  - Data structures
    - `data.table` (or `dtplyr`)
  - Sensitivity analysis
  - Reproducibility (random numbers)
    - see [dust](https://mrc-ide.github.io/dust/articles/rng.html)

# Installation (TBD)

## Macos

For parallelization, you need to [enable OpenMP support](https://github.com/Rdatatable/data.table/wiki/Installation) for `data.table` (ideally following the recommended procedure: "Option 1.1: Install libomp with brew").
