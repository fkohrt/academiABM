#include <Rcpp.h>
using namespace Rcpp;

//' Trim a vector of integers.
//'
//' Given a vector of integers `x`, remove entries at the beginning and the end
//' as indicated by `drop_first` and `drop_last`.
//'
//' @details This is faster than base R subsetting when the vector is long and
//'   there are many entries to be removed, as no time is lost for allocating
//'   index vectors. Implementation follows
//'   <https://stackoverflow.com/q/57334741>.
//'
//' @param x A vector of integers.
//' @param drop_first,drop_last How many entries to remove from the beginning /
//'   the end of `x`.
//' @return A subset of `x` without the first and last entries.
//' @noRd
// [[Rcpp::export]]
IntegerVector trim_vector(IntegerVector x, int drop_first, int drop_last) {
  return IntegerVector(x.begin() + drop_first, x.end() - drop_last);
}

//' Remove entries from a vector of integers.
//'
//' Given a vector of integers `x`, remove the entries indicated by an index
//' vector `neg_idx`.
//'
//' @details Implementation follows  <https://stackoverflow.com/q/57334741>.
//'
//' @param x A vector of integers.
//' @param neg_idx A vector of integers indicating the entries to be removed
//'   from `x`.
//' @return A subset of `x` without the elements `neg_idx`.
//' @noRd
// [[Rcpp::export]]
IntegerVector efficient_negative_indexing(IntegerVector x, IntegerVector neg_idx) {
  std::sort(neg_idx.begin(), neg_idx.end());
  size_t ni_size = neg_idx.size();
  size_t xsize = x.size();
  int * xptr = INTEGER(x);
  size_t xtposition = 0;
  IntegerVector xt(xsize - ni_size); // allocate new vector of the correct size
  int * xtptr = INTEGER(xt);
  int range_begin, range_end;
  for(size_t i=0; i < ni_size; ++i) {
    if(i == 0) {
      range_begin = 0;
    } else {
      range_begin = neg_idx[i-1];
    }
    range_end = neg_idx[i] - 1;
    // std::cout << range_begin << " " << range_end << std::endl;
    std::copy(xptr+range_begin, xptr+range_end, xtptr+xtposition);
    xtposition += range_end - range_begin;
  }
  std::copy(xptr+range_end+1, xptr + xsize, xtptr+xtposition);
  return xt;
}

//' Punch a vector of integers.
//'
//' Given a vector of integers `x`, remove its inner part as indicated by
//' `keep_first` and `keep_last`.
//'
//' @details This is faster than base R subsetting when the vector is long and
//'   there are many entries to be removed, as no time is lost for allocating
//'   index vectors. Implementation inspired by
//'   <https://stackoverflow.com/q/57334741>.
//'
//' @param x A vector of integers.
//' @param keep_first,keep_last How many entries to keep from the beginning /
//'   the end of `x`.
//' @return A subset of `x` containing only the first and last entries.
//' @noRd
// [[Rcpp::export]]
IntegerVector punch_vector(IntegerVector x, int keep_first, int keep_last) {
  size_t xsize = x.size();
  int * xptr = INTEGER(x);

  IntegerVector xt(keep_first + keep_last); // allocate new vector of the correct size
  int * xtptr = INTEGER(xt);

  if(keep_first > 0) {
    std::copy(xptr, xptr + keep_first, xtptr);
  }
  if (keep_last > 0) {
    std::copy(xptr + xsize - keep_last, xptr + xsize, xtptr + keep_first);
  }
  return xt;
}

//' Punch a vector of integers and swap first and last part.
//'
//' Given a vector of integers `x`, remove its inner part as indicated by
//' `keep_first` and `keep_last`, but return the first and last part swapped.
//'
//' @details This is faster than base R subsetting when the vector is long and
//'   there are many entries to be removed, as no time is lost for allocating
//'   index vectors. Implementation inspired by
//'   <https://stackoverflow.com/q/57334741>.
//'
//' @param x A vector of integers.
//' @param keep_first,keep_last How many entries to keep from the beginning /
//'   the end of `x`.
//' @return A subset of `x` containing only the first and last entries, but
//'   swapped: first the last entries, then the first entries.
//' @noRd
// [[Rcpp::export]]
IntegerVector punch_and_swap_vector(IntegerVector x, int keep_first, int keep_last) {
  size_t xsize = x.size();
  int * xptr = INTEGER(x);

  IntegerVector xt(keep_first + keep_last); // allocate new vector of the correct size
  int * xtptr = INTEGER(xt);

  if (keep_last > 0) {
    std::copy(xptr + xsize - keep_last, xptr + xsize, xtptr);
  }
  if(keep_first > 0) {
    std::copy(xptr, xptr + keep_first, xtptr + keep_last);
  }
  return xt;
}
