#' Get utilised rows of an entities object depending on its internals
#'
#' @param first The row of the earliest added entity.
#' @param last The row of most recently added entity.
#' @param size How many entities are currently stored.
#' @param capacity How many entities can be stored before the store needs to
#'   grow.
#' @param capacity_v Capacity vector for faster subsetting.
#' @return The utilised rows of an entities object in the order of their
#'   addition.
#' @noRd
get_utilised_rows <- function(first,
                              last,
                              size,
                              capacity,
                              capacity_v) {
  # nolint start
  #
  # We could calculate the utilised rows like this:
  #
  #   `utilised_rows <- mod_one(seq_len(private$.size) +
  #                             private$.first - 1L,
  #                             private$.capacity)`
  #   `private$.dt[utilised_rows, attribute, with = FALSE]`
  #
  # But `seq_len` takes time to allocate, so we subset from existing
  # `capacity_v`, using Rcpp. Read on:
  #   - <https://stackoverflow.com/q/57334741>
  #   - <https://stackoverflow.com/a/57365227>
  #   - <https://stackoverflow.com/q/2778510>
  #
  # nolint end

  next_available_row <- mod_one(last + 1L, capacity)

  if (next_available_row > first) {
    # capacity_v can be trimmed from start and end to fit
    trim_vector(
      capacity_v,
      first - 1L,
      capacity - last
    )
  } else if (next_available_row < first) {
    # capacity_v cannot be trimmed but we need to punch out an inner part
    punch_and_swap_vector(
      capacity_v,
      next_available_row - 1L,
      capacity - first + 1L
    )
  } else if (size > 0L) {
    # At full capacity
    punch_and_swap_vector(
      capacity_v,
      next_available_row - 1L,
      capacity - first + 1L
    )
  } else {
    # No entities stored so far
    integer(0)
  }
}
