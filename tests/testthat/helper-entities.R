#' Get an empty entities object
#'
#' @param attribute_count How many attributes the entities object should have.
#' @param capacity The capacity of the entities object.
#' @return An empty entities object
get_empty_entities <- function(attribute_count = 5L,
                               capacity = 10L) {
  stopifnot(attribute_count <= 26)

  types <- list(NA, NA_integer_, NA_real_, NA_complex_, NA_character_)

  if (attribute_count <= 5L) {
    attribs <- sample(types, attribute_count)
    names(attribs) <- letters[seq_len(attribute_count)]
    entities$new(attributes = attribs, capacity = capacity)
  } else {
    attribs <- sample(types, attribute_count, replace = TRUE)
    names(attribs) <- letters[seq_len(attribute_count)]
    entities$new(attributes = attribs, capacity = capacity)
  }
}

#' Get one or many random values
#'
#' @param types A character vector indicating the type of value to get:
#'   logical, integer, double, complex, character
#' @return A list of random values of the specified type(s).
get_random_value <- function(types) {
  lapply(
    types,
    function(type) {
      switch(type,
        logical = miranda::runif1_romutrio() < 0.5,
        integer = sample.int(100, size = 1, replace = TRUE),
        double = miranda::runif1_romutrio(),
        complex = sample.int(100, size = 1, replace = TRUE) +
          sample.int(100, size = 1, replace = TRUE) * 1i,
        character = paste0(sample(letters, size = 5, replace = TRUE),
          collapse = ""
        )
      )
    }
  )
}

#' Get types of an entities object
#'
#' @param e An entities object.
#' @return A vector of characters indicating the types of the attributes of `e`.
get_types <- function(e) {
  as.character(lapply(e$get(), typeof))
}

#' Add random entities
#'
#' @param e The entities object to which to add random entities.
#' @param count The number of random entities to add.
#' @return Nothing.
add_random_entities <- function(e, count = 10L) {
  types <- get_types(e)
  for (i in seq_len(count)) {
    e$add_entities(get_random_value(types))
  }
  invisible(NULL)
}
