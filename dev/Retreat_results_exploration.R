#detach(package:academiABM)
remove.packages("academiABM")
remotes::install_local(
  path = "~/Documents/academiABM",
  force = TRUE,
  upgrade = "never",
  dependencies = FALSE
)
unloadNamespace("academiABM")
doFuture::registerDoFuture()
if (Sys.getenv("RSTUDIO") == "1") {
  future::plan(future::multisession)
} else {
  future::plan(future::multicore)
}

options(progressr.enable = TRUE) # report on progress in non-interactive mode
progressr::handlers(global = TRUE)
progressr::handlers("cli")

set.seed(42L)
seeds <- dqrng::generateSeedVectors(nseeds = 2L)
miranda::set_seed_romutrio(seedlo = seeds[[1L]][[1L]], seedhi = seeds[[1L]][[2L]])
dqrng::dqset.seed(seed = seeds[[2L]])

agent_count <- 100L

# e <- academiABM::ensemble$new(
#   agents_count = agent_count,
#   prob_open_original = rep(0, agent_count), # vary this
#   prob_open_repeating = rep(0, agent_count), # vary this
#   target_power = rep(NA_real_, agent_count), # vary this
#   prob_reproduce = rep(0, agent_count),
#   prob_replicate = rep(0.125, agent_count) # vary this
# )
#
# e$run(
#   utilize_future = TRUE,
#   iteration_count = 5,
#   steps_count = 25,
#   p_H1 = 0.1, # vary this
#   alpha = 0.05,
#   prob_closed_has_data = 0.25, # vary this
#   assumed_effect = academiABM:::r_to_d(0.19), # vary this
#   prob_analysis_error = 0,
#   prob_attempt_p_hacking = 0.2, # vary this
#   duration_base_original = 2L,
#   duration_base_repeating = 1L,
#   duration_open = 1L,
#   duration_per_observation = (10 * 5 * 4)^-1,
#   prob_pub_sig = 1, # vary this
#   prob_pub_null = 1, # vary this
#   prob_pub_conf = 1, # vary this
#   prob_pub_disconf = 1 # vary this
# )
#
# sim_plot <- e$plot(c(
#   "paper_count",
#   "hypothesis_FNR", # The fraction of false negatives among all published negative hypotheses.
#   "hypothesis_TPR", # The fraction of true positives among all published positive hypotheses.
#   "open_rate", # The fraction of open papers among all published papers.
#   "paper_FNR", # The fraction of false negatives among all published negative original and replication studies.
#   "paper_TPR", # Fraction of true positives among all published positive original and replication studies.
#   "replication_rate" # Fraction of replication papers among all published papers.
# ))

ggplot2::ggsave("sim_plot.pdf", sim_plot, device = cairo_pdf)

p_hacking_without_pb <- list(shape = list(
  agents_count = agent_count,
  prob_open_original = rep(0, agent_count),
  prob_open_repeating = rep(0, agent_count),
  target_power = rep(NA_real_, agent_count),
  prob_reproduce = rep(0, agent_count)#,
  #prob_replicate = rep(0.125, agent_count)
), sweep = list(
  shape_params = list(prob_replicate = seq(0, 1, 0.1)),
  environment_params = list(prob_attempt_p_hacking = seq(0, 1, 0.1)),
  iteration_count = 50,
  steps_count = 25,
  datapoints_count = 1,
  p_H1 = 0.1,
  alpha = 0.05,
  prob_closed_has_data = 0.25,
  assumed_effect = academiABM:::r_to_d(0.19),
  prob_analysis_error = 0,
  #prob_attempt_p_hacking = 0.2,
  duration_base_original = 2L,
  duration_base_repeating = 1L,
  duration_open = 1L,
  duration_per_observation = (10 * 5 * 4)^-1,
  prob_pub_sig = 1,
  prob_pub_null = 1,
  prob_pub_conf = 1,
  prob_pub_disconf = 1
))

e <- do.call(what = academiABM::experiment$new, args = p_hacking_without_pb$shape)
do.call(what = e$sweep, args = p_hacking_without_pb$sweep)

sim_plot <- e$plot(measures = c(
  "bandwidth",
  "confirmation_rate",
  "depth", # The average number of observations collected in published studies for published hypotheses.
  "open_rate", # The fraction of open papers among all published papers.
  "paper_count",
  "hypothesis_FNR", # The fraction of false negatives among all published negative hypotheses.
  "hypothesis_TPR", # The fraction of true positives among all published positive hypotheses.
  "paper_FNR", # The fraction of false negatives among all published negative original and replication studies.
  "paper_TPR", # Fraction of true positives among all published positive original and replication studies.
  "replication_rate" # Fraction of replication papers among all published papers.
), focal_parameter = "prob_attempt_p_hacking")

ggplot2::ggsave(
  "p_hacking_without_pb.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR", "paper_TPR", "paper_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("prob_replicate", "prob_attempt_p_hacking"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "p_hacking_without_pb-prob_replicate.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

p_hacking_with_pb <- list(shape = list(
  agents_count = agent_count,
  prob_open_original = rep(0, agent_count),
  prob_open_repeating = rep(0, agent_count),
  target_power = rep(NA_real_, agent_count),
  prob_reproduce = rep(0, agent_count)#,
  #prob_replicate = rep(0.125, agent_count)
), sweep = list(
  shape_params = list(prob_replicate = seq(0, 1, 0.1)),
  environment_params = list(prob_attempt_p_hacking = seq(0, 1, 0.1)),
  iteration_count = 50,
  steps_count = 25,
  datapoints_count = 1,
  p_H1 = 0.1,
  alpha = 0.05,
  prob_closed_has_data = 0.25,
  assumed_effect = academiABM:::r_to_d(0.19),
  prob_analysis_error = 0,
  #prob_attempt_p_hacking = 0.2,
  duration_base_original = 2L,
  duration_base_repeating = 1L,
  duration_open = 1L,
  duration_per_observation = (10 * 5 * 4)^-1,
  prob_pub_sig = 1,
  prob_pub_null = 0.009,
  prob_pub_conf = 0.6,
  prob_pub_disconf = 0.7
))

e <- do.call(what = academiABM::experiment$new, args = p_hacking_with_pb$shape)
do.call(what = e$sweep, args = p_hacking_with_pb$sweep)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("prob_attempt_p_hacking", "prob_replicate"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "p_hacking_with_pb-prob_attempt_p_hacking.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR", "paper_TPR", "paper_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("prob_replicate", "prob_attempt_p_hacking"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "p_hacking_with_pb-prob_replicate.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

target_power_with_pb <- list(shape = list(
  agents_count = agent_count,
  prob_open_original = rep(0, agent_count),
  prob_open_repeating = rep(0, agent_count),
  #target_power = rep(NA_real_, agent_count),
  prob_reproduce = rep(0, agent_count),
  prob_replicate = rep(0.125, agent_count)
), sweep = list(
  shape_params = list(target_power = seq(0.1, 1, 0.1)),
  environment_params = list(assumed_effect = seq(0.1, 1, 0.1)),
  iteration_count = 10,
  steps_count = 25,
  datapoints_count = 1,
  p_H1 = 0.1,
  alpha = 0.05,
  prob_closed_has_data = 0.25,
  #assumed_effect = academiABM:::r_to_d(0.19),
  prob_analysis_error = 0,
  prob_attempt_p_hacking = 0.2,
  duration_base_original = 2L,
  duration_base_repeating = 1L,
  duration_open = 1L,
  duration_per_observation = (10 * 5 * 4)^-1,
  prob_pub_sig = 1,
  prob_pub_null = 0.009,
  prob_pub_conf = 0.6,
  prob_pub_disconf = 0.7
))

e <- do.call(what = academiABM::experiment$new, args = target_power_with_pb$shape)
do.call(what = e$sweep, args = target_power_with_pb$sweep)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("target_power", "assumed_effect"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "target_power_with_pb-target_power.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("assumed_effect", "target_power"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "target_power_with_pb-assumed_effect.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

openness <- list(shape = list(
  agents_count = agent_count,
  #prob_open_original = rep(0, agent_count),
  prob_open_repeating = rep(1, agent_count),
  target_power = rep(NA_real_, agent_count),
  prob_reproduce = rep(0, agent_count)#,
  #prob_replicate = rep(0.125, agent_count)
), sweep = list(
  shape_params = list(prob_open_original = seq(0, 1, 0.1), prob_replicate = seq(0, 1, 0.1)),
  environment_params = list(),
  iteration_count = 10,
  steps_count = 25,
  datapoints_count = 1,
  p_H1 = 0.1,
  alpha = 0.05,
  prob_closed_has_data = 0.25,
  assumed_effect = academiABM:::r_to_d(0.19),
  prob_analysis_error = 0,
  prob_attempt_p_hacking = 0.2,
  duration_base_original = 2L,
  duration_base_repeating = 1L,
  duration_open = 1L,
  duration_per_observation = (10 * 5 * 4)^-1,
  prob_pub_sig = 1,
  prob_pub_null = 0.009,
  prob_pub_conf = 0.6,
  prob_pub_disconf = 0.7
))

e <- do.call(what = academiABM::experiment$new, args = openness$shape)
do.call(what = e$sweep, args = openness$sweep)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("prob_replicate", "prob_open_original"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "openness-prob_replicate.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)

sim_plot <- lapply(
  X = c("bandwidth", "depth", "hypothesis_TPR", "hypothesis_FNR"),
  FUN = function(measure) {
    e$plot(measures = measure, focal_parameter = c("prob_open_original", "prob_replicate"))
  }
) |>
  patchwork::wrap_plots()

ggplot2::ggsave(
  "openness-prob_open_original.pdf",
  sim_plot + patchwork::plot_layout(ncol = 1),
  width = 15,
  height = 10 * (length(sim_plot$patches$plots) + !patchwork:::is_empty(sim_plot)),
  units = "cm",
  limitsize = FALSE,
  device = cairo_pdf
)
