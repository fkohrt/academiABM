test_that("a single simulation can be run without error", {
  expect_error(
    {
      sim <- simulation$new()
      sim$run()
      print(sim$plot())
      print(sim$report_total)
    },
    NA
  )
})
