---
title: Non-additive errors
lang: en
format:
  html:
    embed-resources: true
    fig-format: svg
---

# Introduction

When basing claims about a population only on a subset of all possible observations, the statistical procedure allows to attach a confidence rating to them, e.g. by taking into account the sample size or the bias of the measurement tool. In all but the simplest cases there is room for decisions that affect this confidence, opening the door to directed biases via p-hacking. But complex methods also invite human error, creating additional uncertainty that is not commonly controlled for. The following is a discussion of these inadvertent deviations from the researcher's intention.

We assume that most erroneous results are correlated to the truth they deviate from and build a normal distribution when they have additive nature. This corresponds to the theoretical finding of the central limit theorem.

```{r}
n <- 10000000

correlated <- dqrng::dqrnorm(n = n, mean = academiABM:::r_to_d(0.19), sd = 1)
h_correlated <- hist(correlated[correlated > -4 & correlated < 4], plot = FALSE, breaks = seq(-4, 4, 0.01))
prettyB::plot_p(
  x = h_correlated$mids,
  y = h_correlated$density,
  type = "l",
  xlim = c(-4, 4),
  xlab = "Cohen's d",
  ylim = c(0, 1),
  ylab = "Estimated density",
  col = rgb(0,0,1,1),
  main = "Correlated erroneous results"
)
abline(v = academiABM:::r_to_d(0.19), lty = 2)
```

Some erroneous results, however, will also have no association whatsoever to the truth they deviate from. This happens, for example, if we inadvertently work with incorrect data. Note that the following figure does not show a uniform distribution because we make a uniform draw in the space of correlation coefficients and then convert it to the space of Cohen's d:

```{r}
uncorrelated <- academiABM:::r_to_d(dqrng::dqrunif(n = n, min = -1, max = 1))
h_uncorrelated <- hist(uncorrelated[uncorrelated > -4 & uncorrelated < 4], plot = FALSE, breaks = seq(-4, 4, 0.01))
prettyB::plot_p(
  x = h_uncorrelated$mids,
  y = h_uncorrelated$density,
  type = "l",
  xlim = c(-4, 4),
  xlab = "Cohen's d",
  ylim = c(0, 1),
  ylab = "Estimated density",
  col = rgb(1,0,0,1),
  main = "Uncorrelated erroneous results"
)
abline(v = academiABM:::r_to_d(0.19), lty = 2)
```

Deviations will therefore be a weighted combination of correlated and uncorrelated results:

```{r}
#' Deviate effect size
#' 
#' Deviate effect sizes using a composite distribution of correlated and
#' uncorrelated errors.
deviate <- function(d, sd = 1, cor_weight = 0.8) {
  n <- length(d)
  
  correlated <- dqrng::dqrnorm(n = n, mean = 0, sd = sd) + d
  uncorrelated <- academiABM:::r_to_d(dqrng::dqrunif(n = n, min = -1, max = 1))
  
  compound <- cor_weight * correlated + (1-cor_weight) * uncorrelated
  
  return(compound)
}
```

The correlated results have weight 0.8 and the uncorrelated results have weight 0.2. The compound distribution is closer to zero compared to the distribution of the correlated erroneous results alone, as can be verified by looking at the resulting unimodal distribution:

```{r}
deviate(d = rep(academiABM:::r_to_d(0.19), 10000000)) |>
  (\(x) x[x > -4 & x < 4])() |>
  prettyB::hist_p(
    freq = FALSE,
    breaks = seq(-4, 4, 0.01),
    xlim = c(-4, 4),
    xlab = "Cohen's d",
    ylim = c(0, 1),
    ylab = "Estimated density",
    main = "Compound and correlated results",
    border = rgb(0, 0, 0, 0),
    col = rgb(1,0,1,1),
    density = 20
  )
lines(h_correlated$mids, h_correlated$density, col = rgb(0,0,1,1))
abline(v = academiABM:::r_to_d(0.19), lty = 2)
legend(x = "topleft", legend = c("Compound", "Only correlated results"), col = c(rgb(1,0,1,1), rgb(0,0,1,1)), lty = 1, fill = c(rgb(1,0,1,1), rgb(0,0,1,1)), density = c(20, NA_real_), bty = "n")
```

Whether agents deem their result plausible depends on which value they expect and how fast they reject deviations. The latter is modeled with a normal distribution which determines the probability to accept results.

```{r}
#' Is effect size deemed plausible?
#' 
#' An effect will be deemed plausible or not with a probability depending on the
#' mean and standard deviation of a normal distribution.
deems_plausible <- function(d,
                            expected_mean = academiABM:::r_to_d(0.19),
                            expected_sd = 0.25) {
  # Calculate global maximum of a normal distribution's PDF given the SD
  max_val <- 1 / (sqrt(2*pi) * expected_sd)
  
  # Observed effect size under expected effect size distribution
  p <- dnorm(x = d, mean = expected_mean, sd = expected_sd)
  
  return(dqrng::dqrunif(
    n = length(d),
    min = 0,
    max = 1
  ) * max_val <= p)
}
```

Agents receive correct or erroneous results pursuant to the following assumptions:

- Agents make a finite amount of errors that create normal distributed results
- The errors are unordered (i.e., the first error does not necessarily cause the most deviation)
  - Errors can be ordered by setting `pow > 0`
- Until agents have found all errors, they will receive erroneous results
- Whether agents find errors depends on their assessment of the likelihood of their erroneous result, which in turn depends on their scrutiny (diligence)
- Agents receive the true value after all errors are fixed

```{r}
#' Helper for [simulate_analysis()] which calculates the new effect sizes
#' 
#' @param d Unbiased Cohen's d as present in raw data.
#' @param error_count A integer vector indicating how many errors agents make
#'   before finding the correct result.
#' @param diligence A numeric vector indicating how fast agents reject results
#'   that differ from their expectations, either with the same length as `d` or
#'   of length `1`. Acceptable values range from (including) `0` to (excluding)
#'   `1`.
#' @param pow To what degree results deviate less from the truth with increasing
#'   attempts. Minimum `0`, maximum `1`.
#' @param heightened_diligence_mode Whether agents will use a normal
#'   distribution with a halved standard deviation to assess their result
#'   after they found their first error.
#' @param cor_weight The weight of the correlated results vs. the uncorrelated
#'   results.
#' @return A list containing the possibly flawed result of a simulated t-test:
#'
#'   - `d`: Cohen's d as (possibly incorrectly) calculated from the sampled
#'     data.
#'   - `has_error`: Whether the analysis is erroneous.
#' @noRd
simulate_analysis_effects <- function(d,
                                      error_count,
                                      diligence,
                                      deviation_sd = 1,
                                      pow = 0,
                                      heightened_diligence_mode = FALSE,
                                      cor_weight = 0.8) {
  n <- length(d)
  if (length(diligence) != n) diligence <- rep_len(diligence, n)
  
  attempt <- 1L
  observed_effect_size <- rep(NA_real_, n)
  found_error_count <- rep(0L, n)
  # observation_history <- as.list(d) # TODO: remove this
  # During this loop, agents only draw erroneous results and either settle on
  # them or don't
  while (any(searching_errors <- attempt <= error_count & is.na(observed_effect_size))) {
    observation <- deviate(d = d[searching_errors], sd = deviation_sd * ((2^(error_count-(attempt-1)))^pow) / ((2^(attempt-1))^pow), cor_weight = cor_weight)
    # observation_history[searching_errors] <- lapply(seq_along(observation_history[searching_errors]), function(i) {c(observation_history[searching_errors][[i]], observation[[i]])}) # TODO: remove this
    
    accepting <- deems_plausible(d = observation, expected_sd = (1 - diligence[searching_errors]) / diligence[searching_errors] / if(heightened_diligence_mode && attempt > 1L) 2 else 1)
    if (anyNA(accepting)) browser()
    if (any(accepting)) {
      observed_effect_size[searching_errors][accepting] <- observation[accepting]
    }
    found_error_count[searching_errors][!accepting] <- found_error_count[searching_errors][!accepting] + 1L
    
    attempt <- attempt + 1L
  }
  
  correct <- is.na(observed_effect_size)
  observed_effect_size[correct] <- d[correct]
  
  return(list(
    d = observed_effect_size,
    has_error = !correct,
    found_error_count = found_error_count
  ))
}
```

Agents differ in the amount of errors they make and in the diligence when assessing their results.

We also need wrapper functions that help with calculations and drawing:

```{r}
#' Simulate analysis of sampled data
#'
#' Simulate the flawed analysis of raw sampled data provided as Cohen's d from
#' which the result of a t-test is calculated.
#'
#' @param d See [simulate_analysis_effects].
#' @param sample_size The number of independent observations sampled for each
#'   group.
#' @param alpha If a p-value is smaller than this probability threshold the
#'   effect will be regarded as significant.
#' @param type See [sample_data()].
#' @param alternative Specify the alternative hypotheses as follows:
#'
#'   - `two.sided`: Undirected / two-tailed test ("Treatment differs from
#'     control.")
#'   - `less`: Directed / one-tailed test ("Treatment is inferior to control.")
#'   - `greater`: Directed / one-tailed test ("Treatment is superior to
#'     control.")
#' @param error_count See [simulate_analysis_effects].
#' @param diligence See [simulate_analysis_effects].
#' @return A list containing the possibly flawed result of a simulated t-test:
#'
#'   - `d`: Cohen's d as (possibly incorrectly) calculated from the sampled
#'     data.
#'   - `p_value`: The probability of `d` if there is no effect.
#'   - `is_significant`: Whether the effect is regarded as statistically
#'     significant.
#'   - `has_error`: Whether the analysis is erroneous.
#' @noRd
simulate_analysis <- function(d,
                              sample_size,
                              alpha,
                              type,
                              alternative,
                              error_count,
                              diligence,
                              deviation_sd = 1,
                              pow = 0,
                              heightened_diligence_mode = FALSE,
                              cor_weight = 0.8) {
  result <- simulate_analysis_effects(
    d = d,
    error_count = error_count,
    diligence = diligence,
    deviation_sd = deviation_sd,
    pow = pow,
    heightened_diligence_mode = heightened_diligence_mode,
    cor_weight = cor_weight
  )
  
  # Calculate p-value corresponding to erroneous d
  group_count <- switch(type,
    two.sample = 2L,
    one.sample = 1L,
    stop(paste0("Invalid test type: ", type))
  )
  df <- group_count * (sample_size - 1L)
  p_value <- academiABM:::t_to_p(
    t_statistic = academiABM:::d_to_t(
      d = d,
      sample_size = sample_size,
      group_count = group_count
    ),
    df = df,
    alternative = alternative
  )
  
  # Calculate new significance
  is_significant <- p_value <= alpha
  
  return(list(
    d = result$d,
    p_value = p_value,
    is_significant = is_significant,
    has_error = result$has_error
  ))
}

plot_data <- function(hist_list, title = NULL, legend = NULL, xlim = NULL, ylim = NULL) {
  colours <- grDevices::hcl.colors(
    n = length(hist_list),
    palette = "Viridis"
  )
  linetypes <- seq_along(hist_list)
  
  for (i in seq_along(hist_list)) {
    if (i == 1) {
      plot(
        x = hist_list[[i]]$mids,
        y = hist_list[[i]]$density,
        type = "l",
        xlim = xlim,
        xlab = "Cohen's d",
        ylim = ylim,
        ylab = "Estimated density",
        col = colours[[i]],
        lty = linetypes[[i]],
        main = title
      )
      if (!is.null(legend)) {
        legend(
          x = "topleft",
          legend = legend,
          col = colours,
          lty = linetypes,
          fill = colours,
          bty = "n"
        )
      }
    } else {
      lines(hist_list[[i]]$mids, hist_list[[i]]$density, col = colours[[i]], lty = linetypes[[i]])
    }
  }
}

plot_analysis <- function(param_name, param_values, other_params = list(), datapoint_count = 10000000L, xlim = c(-4, 4), ylim = c(0, 1)) {
  defaults <- list(
    d = academiABM:::r_to_d(0.19),
    error_count = 2,
    diligence = 0.8
  ) |> modifyList(other_params)
  
  data <- list()
  for (i in seq_along(param_values)) {
    defaults |>
      within(assign(param_name, param_values[[i]])) |>
      within(if (length(d) == 1) assign("d", rep(d, datapoint_count))) |>
      do.call(what = simulate_analysis_effects) |>
      getElement("d") -> observed
    h <- hist(
      x = observed[observed > xlim[[1]] & observed < xlim[[2]]],
      plot = FALSE,
      breaks = seq(xlim[[1]], xlim[[2]], 0.01)
    )
    data <- c(data, list(h))
  }
  
  plot_data(data, title = paste0("Varying ", param_name), legend = param_values, ylim = ylim)
  
  invisible(data)
}
```

# Results

The (partly erroneous) observed effect sizes build a spike-and-slab distribution centered around the sampled effect size:

```{r}
data <- simulate_analysis_effects(d = rep(academiABM:::r_to_d(0.19), 10000000), diligence = 0.8, error_count = 2L) |>
  getElement("d")
data <- hist(x = data[data > -4 & data < 4], plot = FALSE, breaks = seq(-4, 4, 0.01))
plot_data(list(data), ylim = c(0, 1))
```

```{r}
data <- simulate_analysis_effects(d = academiABM:::draw_true_effect_sizes(n = 10000000), diligence = 0.8, error_count = 2L) |>
  getElement("d")
data <- hist(x = data[data > -4 & data < 4], plot = FALSE, breaks = seq(-4, 4, 0.01))
plot_data(list(data), ylim = c(0, 2))
```

# Varying parameters

Making more errors decreases the mass of the spike:

```{r}
data <- plot_analysis("error_count", 0:4)
# data[[1]][["density"]][[439]] * 0.01
```

Increasing the diligence increases the mass of the spike and decreases the spread of the slab. With a higher number of errors it also takes a higher diligence for the spike to be visible at all:

```{r}
data <- plot_analysis("diligence", seq(0, 1, 0.1), other_params = list(error_count = 2))
# sum(data[[2]][["density"]][-439] * 0.01)
```

```{r}
plot_analysis("diligence", seq(0, 1, 0.1), datapoint_count = 10000000, other_params = list(error_count = 100))
```

For low values of diligence no spike is visible, and a higher standard deviation of the deviating results increases the spread of the slab.

TODO: Choose deviation_sd by looking into resulting published effects and choose plausible value.

- Welches ganz konkrete Modell?
  - (1) additive Fehler?
  - (3) Parameterwahl
- (2) Performance-Frage -> Lookup-Table
  - dann Mischverteilung

```{r}
plot_analysis("deviation_sd", seq(0, 3, 0.3), other_params = list(cor_weight = 1, diligence = 0))
```

For high values of diligence, a higher standard deviation of the deviating results shifts the mass from the slab to the spike, as results are more likely to be rejected until the sampled effect size is obtained (= spike).

```{r}
plot_analysis("deviation_sd", seq(0, 3, 0.3), other_params = list(cor_weight = 1, diligence = 0.8))
```

The heightened diligence mode once one error has been found has the effect of shifting a moderate amount of mass from the slab to the spike.

```{r}
plot_analysis("heightened_diligence_mode", c(FALSE, TRUE))
```

For low values of diligence the expected mean has little influence and the sampled effect size `d` determines the mode of both spike and slab.

```{r}
plot_analysis("d", seq(0, 1, 0.2), other_params = list(diligence = 0.2))
```

For high values of diligence the expected mean dominates the mode of the slab, whereas the spike is determined by the sampled effect size `d`.

```{r}
plot_analysis("d", seq(0, 1, 0.2), other_params = list(diligence = 0.8))
```

If erroneous results become ordered such that subsequent errors have a smaller standard deviation than early errors (`sd = 1`), mass is being shifted from the spike to the slab. If, in contrast, early errors have a larger standard deviation than subsequent errors (`sd = 1`), mass is being shifted in the opposite direction (not visible here, depends on the implementation of `pow`). Finally, if errors are ordered such that early errors are larger and subsequent errors are smaller than the medium sized errors (`sd = 1`) then there is no simple pattern: Mass first goes from slab to spike and then the opposite direction.

The mass shifting is only visible if the diligence is high enough to not accept the first error of many. In addition, the differences appear with `cor_weight` approaching 1.

```{r}
plot_analysis("pow", seq(0, 1, 0.2), other_params = list(error_count = 5, diligence = 0.8, cor_weight = 1), ylim = c(0, 2))
```

For low values of diligence, increasing the weight of the correlated erroneous results moves the mode to the sampled effect size and decreases the spread.

```{r}
plot_analysis("cor_weight", seq(0, 1, 0.1), other_params = list(diligence = 0))
```

For high values of diligence, increasing the weight of the correlated erroneous results only moves mass from the spike into the slab.

```{r}
plot_analysis("cor_weight", seq(0, 1, 0.1), other_params = list(diligence = 0.8))
```

The performance is worse than with the previous algorithm. It can be improved by creating a custom distribution function, but the effect of varying `d`, `diligence` and `error_count` is non-trivial. For reasons unknown a C++ version has been even slower.

```{r}
observed <- simulate_analysis(
  d = rep(academiABM:::r_to_d(0.19), n),
  sample_size = 100,
  alpha = 0.05/2,
  type = "two.sample",
  alternative = "greater",
  error_count = 2,
  diligence = 0.8
)$d

h <- hist(
      x = observed[observed > -4 & observed < 4],
      plot = FALSE,
      breaks = seq(-4, 4, 0.01)
    )

dat <- data.frame(x = h$mids, y = h$density)

error_d <- pdqr::new_d(dat, type = "continuous")
error_r <- pdqr::new_r(dat, type = "continuous")

# profvis::profvis({error_r(n = n)})

error_r(n = n) |>
  hist(
    freq = FALSE,
    breaks = seq(-4, 4, 0.01),
    xlim = c(-4, 4),
    xlab = "Cohen's d",
    ylim = c(0, 1),
    ylab = "Estimated density",
    main = "Histogram",
    border = rgb(0, 0, 0, 0),
    col = "#4B0055",
    density = 20
)
```

# Explorations

When uncommenting the code with `observation_history`, we can see that errors are not automatically ordered.

```{r}
#| eval: FALSE
observation_history <- simulate_analysis(
    d = rep(academiABM:::r_to_d(0.19), 100),
    sample_size = 100,
    alpha = 0.05/2,
    type = "two.sample",
    alternative = "greater",
    error_count = 10,
    diligence = 0.999,
    pow = 1
)$observation_history

lapply(seq_along(observation_history), function(i) {abs(observation_history[[i]][-1] - observation_history[[i]][[1]])}) |> lapply(function(x) {cor(x = x, y = seq_along(x))}) |> as.numeric() |> na.omit()
```

With `pow = 0`, instances where not all errors have been found are no different:

```{r}
data <- simulate_analysis_effects(d = rep(academiABM:::r_to_d(0.19), 10000000), diligence = 0.4, error_count = 5L, pow = 0, cor_weight = 1)

xlim <- c(-4, 4)
ylim <- c(0, 1)

l <- list()
for (i in seq_len(5)) {
  observed <- data$d[data$found_error_count == i]
  
  h <- hist(
    x = observed[observed > xlim[[1]] & observed < xlim[[2]]],
    plot = FALSE,
    breaks = seq(xlim[[1]], xlim[[2]], 0.01)
  )
  l <- c(l, list(h))
}
plot_data(l, title = "By found errors", legend = seq_len(5), ylim = ylim)
```

Errors can be ordered by setting `pow = 1`:

```{r}
data <- simulate_analysis_effects(d = rep(academiABM:::r_to_d(0.19), 10000000), diligence = 0.4, error_count = 5L, pow = 1, cor_weight = 1)

xlim <- c(-4, 4)
ylim <- c(0, 1)

l <- list()
for (i in seq_len(5)) {
  observed <- data$d[data$found_error_count == i]
  
  h <- hist(
    x = observed[observed > xlim[[1]] & observed < xlim[[2]]],
    plot = FALSE,
    breaks = seq(xlim[[1]], xlim[[2]], 0.01)
  )
  l <- c(l, list(h))
}
plot_data(l, title = "By found errors", legend = seq_len(5), ylim = ylim)
```

- deviation_sd fix
- heightened_mode rauslassen
- diligence kann Ziel von Intervention sein


- Begrüdung für Normalverteilung muss anders lauten
